import React from "react";

const Footer = () => {
  return (
    <section class="footer-section">
      <div class="container">
        <div class="row">
          <div class="col-lg col-md-3 address-footer-content">
            <p>Jalan Suroyo No. 161 Mayangan Kota Probolonggo 672000</p>
            <p>binarcarrental@gmail.com</p>
            <p>081-233-334-808</p>
          </div>
          <div class="col-lg col-md-3 navigation-list-footer">
            <ul>
              <li>
                <a href="#ourservice-section">Our services</a>
              </li>
              <li>
                <a href="#whyus-section">Why Us</a>
              </li>
              <li>
                <a href="#testimonial-section">Testimonial</a>
              </li>
              <li>
                <a href="#faq-section">FAQ</a>
              </li>
            </ul>
          </div>
          <div class="col-lg col-md-3 - sosmed-footer">
            <ul>
              <li>
                <p>Connect with us</p>
              </li>
              <li>
                <img src="Images/icon-facebook.png" alt="" />
              </li>
              <li>
                <img src="Images/icon-instagram.png" alt="" />
              </li>
              <li>
                <img src="Images/icon-twitter.png" alt="" />
              </li>
              <li>
                <img src="Images/icon-mail.png" alt="" />
              </li>
              <li>
                <img src="Images/icon-twitch.png" alt="" />
              </li>
            </ul>
          </div>
          <div class="col-lg col-md-3 - coyright-footer">
            <ul>
              <li>
                <p>Copyright Binar 2022</p>
              </li>
              <li>
                <img src="Images/Rectangle-footer.png" alt="" />
              </li>
            </ul>
          </div>
        </div>
      </div>
    </section>
  );
};

export default Footer;
