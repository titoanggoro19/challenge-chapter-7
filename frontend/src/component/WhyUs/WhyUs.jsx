import React from "react";

const WhyUs = () => {
  return (
    <section className="whyus-section" id="whyus-section">
      <div className="container text-center text-xxl-start text-md-start">
        <h1>Why Us ?</h1>
        <p>Mengapa harus pilih Binar Car Rental?</p>
      </div>
      <div className="section whyus-cardtitle-section">
        <div className="container">
          <div className="row justify-content-center">
            <div className="col-xxl-3 col-md-6 col-lg-3 mt-3">
              <div className="card" style={{ height: "12rem" }}>
                <img
                  src="Images/icon-complete.png"
                  className="card-img-top"
                  alt="..."
                />
                <div className="card-body">
                  <h5 className="card-title">Mobil Lengkap</h5>
                  <p className="card-text">
                    Tersedia banyak pilihan mobil, kondisi masih baru, bersih
                    dan terawat <br />
                    <br />
                  </p>
                </div>
              </div>
            </div>
            <div className="col-xxl-3 col-md-6 col-lg-3 mt-3">
              <div className="card" style={{ height: "12rem" }}>
                <img
                  src="Images/icon-price.png"
                  className="card-img-top"
                  alt="..."
                />
                <div className="card-body">
                  <h5 className="card-title">Harga Murah</h5>
                  <p className="card-text">
                    Harga murah dan bersaing, bisa bandingkan harga kami dengan
                    rental mobil lain
                  </p>
                </div>
              </div>
            </div>
            <div className="col-xxl-3 col-md-6 col-lg-3 mt-3">
              <div className="card" style={{ height: "12rem" }}>
                <img
                  src="Images/icon-24hrs.png"
                  className="card-img-top"
                  alt="..."
                />
                <div className="card-body">
                  <h5 className="card-title">Layanan 24 Jam</h5>
                  <p className="card-text">
                    Siap melayani kebutuhan Anda selama 24 jam nonstop. Kami
                    juga tersedia di akhir minggu
                  </p>
                </div>
              </div>
            </div>
            <div className="col-xxl-3 col-md-6 col-lg-3 mt-3">
              <div className="card" style={{ height: "12rem" }}>
                <img
                  src="Images/icon-professional.png"
                  className="card-img-top"
                  alt="..."
                />
                <div className="card-body">
                  <h5 className="card-title">Sopir Profesional</h5>
                  <p className="card-text">
                    Sopir yang profesional, berpengalaman, jujur, ramah dan
                    selalu tepat waktu <br />
                    <br />
                  </p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  );
};

export default WhyUs;
