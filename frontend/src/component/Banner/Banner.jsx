import React from "react";

const Banner = () => {
  return (
    <section className="ctabanner-section">
      <div className="container text-center d-flex flex-column">
        <div className="cta-banner p-4">
          <h1>Sewa Mobil di (Lokasimu) Sekarang</h1>
          <p>
            Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
            eiusmod <br />
            tempor incididunt ut labore et dolore magna aliqua.
          </p>
          <div className="btn-cta-banner">
            <button
              type="button"
              className="btn btn-success"
              style={{ backgroundColor: "#5cb85f" }}
            >
              Mulai sewa Mobil
            </button>
          </div>
        </div>
      </div>
    </section>
  );
};

export default Banner;
