import React from "react";
import { Link } from "react-router-dom";

const Jumbotron = () => {
  return (
    <section className="navbar-section" style={{ backgroundColor: "#f1f3ff" }}>
      <section className="jumbtron-section">
        <div className="container">
          <div className="jumbotron">
            <div className="row">
              <div className="col-xxl-6 col-md-12 col-lg-6 col-sm-12">
                <h1>Sewa &amp; Rental Mobil Terbaik di kawasan (Lokasimu)</h1>
                <p className="mt-4">
                  Selamat datang di Binar Car Rental. Kami menyediakan mobil
                  kualitas <br />
                  terbaik dengan harga terjangkau. Selalu siap melayani
                  kebutuhanmu
                  <br />
                  untuk sewa mobil selama 24 jam.
                </p>
                <Link to="/cars">
                  <button type="button" className="btn btn-success">
                    Mulai Sewa Mobil
                  </button>
                </Link>
              </div>
              <div className="col-xxl-6 col-md-12 col-lg-6 col-sm-12 d-flex">
                <img src="Images/img-car.png" className="w-100" alt="" />
              </div>
            </div>
          </div>
        </div>
      </section>
    </section>
  );
};

export default Jumbotron;
