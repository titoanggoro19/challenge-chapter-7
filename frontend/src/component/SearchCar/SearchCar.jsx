import React from "react";

const SearchCar = () => {
  return (
    <div className="section-car-section">
      <div
        id="searchColumn"
        className="container d-flex justify-content-center"
      >
        <div
          className="row d-flex justify-content-center search-car"
          style={{ zIndex: 2 }}
          onclick="activeDarkBackground()"
        >
          <div className="col-xxl-2 col-sm-5">
            <div className="mb-3 mt-2">
              <label className="form-label">Tipe Driver</label>
              <select
                className="form-select"
                aria-label="Default select example"
              >
                <option selected>Pilih Tipe Driver</option>
                <option value={1}>Dengan Sopir</option>
                <option value={2}>Tanpa Sopir (Lepas Kunci)</option>
              </select>
            </div>
          </div>
          <div className="col-xxl-2 col-sm-5">
            <div className="mb-3 mt-2">
              <label className="form-label">Tanggal</label>
              <input id="date" type="date" className="form-control" />
            </div>
          </div>
          <div className="col-xxl-2 col-sm-5">
            <div className="mb-3 mt-2">
              <label className="form-label">Waktu Jemput/Ambil</label>
              <input id="time" type="time" className="form-control" />
            </div>
          </div>
          <div className="col-xxl-2 col-sm-5">
            <div className="mb-3 mt-2">
              <label className="form-label">Jumlah Penumpang</label>
              <input
                id="totalpassenger"
                placeholder="Jumlah Penumpang"
                type="number"
                className="form-control"
              />
            </div>
          </div>
          <div className="col-xxl-auto col-sm-5 text-center mt-xxl-3 mt-md-0">
            <div className="mb-3 mt-xxl-4">
              <button
                id="submitbtn"
                onclick="app.init().then(app.run)"
                type="button"
                className="btn btn-success btn-green-custom"
              >
                Pilih Mobil
              </button>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default SearchCar;
