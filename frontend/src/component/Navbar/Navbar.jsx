import React from "react";
import { Link } from "react-router-dom";

function Navbar() {
  return (
    <section className="navbar-section" style={{ backgroundColor: "#f1f3ff" }}>
      <nav
        className="navbar navbar-expand-lg navbar-light fixed-top pt-0 pb-2"
        style={{ backgroundColor: "#f1f3ff" }}
      >
        <div className="container mt-2">
          <Link to="/">
            <a className="navbar-brand" href="#">
              <img src="Images/logo.png" alt="" />
            </a>
          </Link>
          <button
            className="navbar-toggler"
            type="button"
            data-bs-toggle="collapse"
            data-bs-target="#navbarSupportedContent"
            aria-controls="navbarSupportedContent"
            aria-expanded="false"
            aria-label="Toggle navigation"
          >
            <span className="navbar-toggler-icon" />
          </button>
          <div
            className="collapse navbar-collapse justify-content-end"
            id="navbarSupportedContent"
          >
            <ul
              className="navbar-nav mb-2 mb-lg-0"
              style={{ marginRight: "-30px" }}
            >
              <li className="nav-item">
                <a
                  className="nav-link active"
                  aria-current="page"
                  href="#ourservice-section"
                >
                  Our Service
                </a>
              </li>
              <li className="nav-item">
                <a className="nav-link active" href="#whyus-section">
                  Why Us
                </a>
              </li>
              <li className="nav-item">
                <a className="nav-link active" href="#testimonial-section">
                  Testimonial
                </a>
              </li>
              <li className="nav-item">
                <a className="nav-link active" href="#faq-section">
                  FAQ
                </a>
              </li>
              <li className="nav-item">
                <button type="button" className="btn btn-success">
                  Register
                </button>
              </li>
            </ul>
          </div>
          <div id="mySidebar" className="sidebar">
            <a
              href="javascript:void(0)"
              className="closebtn"
              onclick="closeNav()"
            >
              ×
            </a>
            <p className="nav-link active bcr" href="#">
              BCR
            </p>
            <a
              className="nav-link active"
              aria-current="page"
              href="#ourservice-section"
            >
              Our Service
            </a>
            <a className="nav-link active" href="#whyus-section">
              Why Us
            </a>
            <a className="nav-link active" href="#testimonial-section">
              Testimonial
            </a>
            <a className="nav-link active" href="#faq-section">
              FAQ
            </a>
            <button type="button" className="btn btn-success">
              Register
            </button>
          </div>
          <div id="main">
            <button className="openbtn" onclick="openNav()">
              ☰
            </button>
          </div>
        </div>
      </nav>
    </section>
  );
}

export default Navbar;
