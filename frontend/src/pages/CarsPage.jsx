import React from "react";
import Footer from "../component/Footer/Footer";
import JumbotronNoButton from "../component/Jumbotron/JumbotronNoButton";

import Navbar from "../component/Navbar/Navbar";
import SearchCar from "../component/SearchCar/SearchCar";

import { useEffect } from "react";

const CarsPage = () => {
  useEffect(() => {
    document.title = "Cars Page";
  }, []);
  return (
    <div>
      <Navbar />
      <JumbotronNoButton />
      <SearchCar />
      <Footer />
    </div>
  );
};

export default CarsPage;
