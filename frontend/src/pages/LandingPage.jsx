import React from "react";
import Banner from "../component/Banner/Banner";
import Carousel from "../component/Carousel/Carousel";
import Faq from "../component/Faq/Faq";
import Footer from "../component/Footer/Footer";
import Jumbotron from "../component/Jumbotron/Jumbotron";
import Navbar from "../component/Navbar/Navbar";
import OurService from "../component/OurService/OurService";
import WhyUs from "../component/WhyUs/WhyUs";
import { useEffect } from "react";

const LandingPage = () => {
  useEffect(() => {
    document.title = "Homepage";
  }, []);
  return (
    <div>
      <Navbar />
      <Jumbotron />
      <OurService />
      <WhyUs />
      <Carousel />
      <Banner />
      <Faq />
      <Footer />
    </div>
  );
};

export default LandingPage;
