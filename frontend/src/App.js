import { BrowserRouter, Routes, Route } from "react-router-dom";
import "bootstrap/dist/css/bootstrap.min.css";
import "bootstrap/dist/js/bootstrap.min.js";
import "./App.css";
import LandingPage from "./pages/LandingPage";
import CarsPage from "./pages/CarsPage";

function App() {
  return (
    <BrowserRouter>
      <Routes>
        <Route path="/" element={<LandingPage />}></Route>
        <Route path="/cars" element={<CarsPage />}></Route>
      </Routes>
    </BrowserRouter>
  );
}

export default App;
